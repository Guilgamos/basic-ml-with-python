# A Basic tutorial for machine learning in python

### Please download anaconda, it is a tools for learning
- [Windows](https://docs.anaconda.com/anaconda/install/windows)
- [Mac](https://docs.anaconda.com/anaconda/install/mac-os)
- [Linux](https://docs.anaconda.com/anaconda/install/linux)
